package com.propiedades.foursquaredemo.controllers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.propiedades.foursquaredemo.R;
import com.propiedades.foursquaredemo.models.Venue;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    private Context context;
    private ArrayList<Venue> venues = new ArrayList<>();

    public RecyclerViewAdapter(Context context, ArrayList<Venue> venues) {
        this.venues = venues;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.venue_recycler_view_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        Venue venue = venues.get(i);
        if (venue.getPhotoUrl() != null && venue.getPhotoUrl().length() > 0) {
            Glide.with(context).load(venue.getPhotoUrl()).into(viewHolder.imageView);
        }

        viewHolder.nameTextView.setText(venue.getVenueName());
        viewHolder.categoryTextView.setText(venue.getMainCategoryName());
        String fullAddress = venue.getAddress();
        if (venue.getCrossStreet().length() > 0) {
            fullAddress += " (" + venue.getCrossStreet() + ")";
        }
        if (venue.getCity().length() > 0) {
            fullAddress += " " + venue.getCity();
        }
        viewHolder.addressTextView.setText(fullAddress);
    }

    @Override
    public int getItemCount() {
        return venues.size();
    }

    public void setVenues(ArrayList<Venue> venues) {
        this.venues = venues;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView nameTextView;
        TextView categoryTextView;
        TextView addressTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            nameTextView = itemView.findViewById(R.id.nameTextView);
            categoryTextView= itemView.findViewById(R.id.categoryTextView);
            addressTextView= itemView.findViewById(R.id.addressTextView);
        }
    }
}
