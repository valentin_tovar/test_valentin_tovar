package com.propiedades.foursquaredemo.views;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

public class AlertDialogFragment extends DialogFragment {

    public interface AlertDialogFragmentListener {
        void okButtonTapped();
        void cancelButtonTapped();
    }

    private AlertDialogFragmentListener listener;

    private String title;
    private String message;
    private String okButtonLabel;
    private String cancelButtonLabel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        title = arguments.containsKey("title") ? arguments.getString("title") : "";
        message = arguments.containsKey("message") ? arguments.getString("message") : "";
        okButtonLabel = arguments.containsKey("okButtonLabel") ? arguments.getString("okButtonLabel") : "Ok";
        cancelButtonLabel = arguments.containsKey("cancelButtonLabel") ? arguments.getString("cancelButtonLabel") : "Cancelar";
    }

    public void setListener(AlertDialogFragmentListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(okButtonLabel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (listener != null) {
                            listener.okButtonTapped();
                        }
                    }
                })
                .setNegativeButton(cancelButtonLabel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (listener != null) {
                            listener.cancelButtonTapped();
                        }
                    }
                });
        return builder.create();
    }
}
