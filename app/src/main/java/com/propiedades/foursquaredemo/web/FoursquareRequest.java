package com.propiedades.foursquaredemo.web;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.propiedades.foursquaredemo.models.Venue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by condesa on 19/09/17.
 */

public class FoursquareRequest {

    public enum FoursquareServiceType {
        searchVenues,
        getVenuePhoto
    }

    private static final String ENDPOINT = "https://api.foursquare.com/";
    private static final String LAST_UPDATE_DATE = "20181001";//"20151126";
    private static final String LANGUAGE_CODE = "es";

    private static final String FOURSQUARE_CLIENT_ID = "PTMFLIQTT5LFZKASLEFG4FJ4QY1EV5YMVXSGEN5QHNICZL4A";
    private static final String FOURSQUARE_CLIENT_SECRET = "ALN1KIMVOVGWHGBBXRXSQMPRGOIOGC24NQ4QGJHG2LFDSSEU";

    private static final String FOURSQUARE_FOOD_CATEGORY = "4d4b7105d754a06374d81259";

    public interface FoursquareRequestListener {
        void onFoursquareRequestFinished(FoursquareServiceType serviceType, Map<String, Object> result, String errorMessage);
    }

    private FoursquareRequestListener listener;

    public FoursquareRequest() {
        listener = null;
    }

    public void setListener(FoursquareRequestListener listener) {
        this.listener = listener;
    }

    private static FoursquareService getService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(FoursquareService.class);
    }

    interface FoursquareService {
        // @Headers("Accept-Language: es")
        @GET("v2/venues/search")
        Call<JsonObject> searchVenues(
                @Query("ll") String latlng,
                @Query("radius") int radius,
                @Query("limit") int limit,
                @Query("categoryId") String categories,
                @Query("client_id") String clientId,
                @Query("client_secret") String clientSecret,
                @Query("v") String lastUpdate,
                @Query("locale") String languageCode);

        @GET("v2/venues/{venueId}/photos")
        Call<JsonObject> getVenuePhoto(
                @Path("venueId") String venueId,
                @Query("group") String group,
                @Query("limit") int limit,
                @Query("client_id") String clientId,
                @Query("client_secret") String clientSecret,
                @Query("v") String lastUpdate,
                @Query("locale") String languageCode);
    }

    public void searchVenues() {

        Call<JsonObject> venuesCall = getService().searchVenues("19.4270231,-99.1686937", 1000, 16, FOURSQUARE_FOOD_CATEGORY, FOURSQUARE_CLIENT_ID, FOURSQUARE_CLIENT_SECRET, LAST_UPDATE_DATE, LANGUAGE_CODE);

        venuesCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(!response.isSuccessful()) {
                    if (listener != null) {
                        listener.onFoursquareRequestFinished(FoursquareServiceType.searchVenues, null, "Hubo un error con el servicio de Foursquare (" + response.code() + ")");
                    }
                    return;
                }

                JsonObject jsonObject = response.body();
                if (jsonObject.isJsonObject() && jsonObject.has("response")) {
                    JsonObject responseJO = jsonObject.getAsJsonObject("response");
                    if (responseJO.isJsonObject() && responseJO.has("venues")) {
                        JsonArray venuesJO = responseJO.getAsJsonArray("venues");
                        if (venuesJO.isJsonArray()) {
                            ArrayList<Venue> venues = new ArrayList<>();
                            while (venuesJO.size() > 0) {
                                JsonObject venue = venuesJO.remove(0).getAsJsonObject();
                                String venueId = venue.get("id").getAsString();
                                String venueName = venue.get("name").getAsString();
                                String address = "";
                                String crossStreet = "";
                                String city = "";
                                String mainCategoryName = "";

                                if (venue.has("location")) {
                                    JsonObject location = venue.getAsJsonObject("location");
                                    if (location.isJsonObject()) {
                                        if (location.has("address")) {
                                            address = location.get("address").getAsString();
                                        }

                                        if (location.has("crossStreet")) {
                                            crossStreet = location.get("crossStreet").getAsString();
                                        }

                                        if (location.has("city")) {
                                            city = location.get("city").getAsString();
                                        }
                                    }
                                }

                                if (venue.has("categories")) {
                                    JsonArray categories = venue.getAsJsonArray("categories");
                                    if (categories.isJsonArray()) {

                                        while (categories.size() > 0) {
                                            JsonObject category = categories.remove(0).getAsJsonObject();

                                            Boolean primary = category.get("primary").getAsBoolean();
                                            if (primary) {
                                                mainCategoryName = category.get("name").getAsString();
                                                break;
                                            } else if (mainCategoryName.length() == 0) {
                                                mainCategoryName = category.get("name").getAsString();
                                            }
                                        }
                                    }
                                }

                                venues.add(new Venue(venueId, venueName, address, crossStreet, city, mainCategoryName));

                            }

                            if (listener != null) {
                                Map<String, Object> result = new HashMap<>();
                                result.put("venues", venues);
                                listener.onFoursquareRequestFinished(FoursquareServiceType.searchVenues, result, null);
                            }
                            return;
                        }
                    }
                }

                if (listener != null) {
                    listener.onFoursquareRequestFinished(FoursquareServiceType.searchVenues, null, "Ocurrió un error al procesar la información");
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (listener != null) {
                    listener.onFoursquareRequestFinished(FoursquareServiceType.searchVenues, null, "El servidor de Foursquare no pudo ser contactado");
                }
            }
        });
    }

    public void getVenueMainPhoto(final String venueId) {

        Call<JsonObject> venuesCall = getService().getVenuePhoto(venueId, "venue", 1, FOURSQUARE_CLIENT_ID, FOURSQUARE_CLIENT_SECRET, LAST_UPDATE_DATE, LANGUAGE_CODE);

        venuesCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(!response.isSuccessful()) {
                    if (listener != null) {
                        if (response.code() == 429) {
                            listener.onFoursquareRequestFinished(FoursquareServiceType.getVenuePhoto, null, "No se pueden obtener las fotos de los establecimientos debido a que se ha alcanzado el límite de solicitudes asociadas a la cuenta de Foursquare");
                        } else {
                            listener.onFoursquareRequestFinished(FoursquareServiceType.getVenuePhoto, null, "Ocurrió un error al intentar obtener las fotos de los establecimientos (" + response.code() + ")");
                        }
                    }
                    return;
                }

                JsonObject jsonObject = response.body();
                if (jsonObject.isJsonObject() && jsonObject.has("response")) {
                    JsonObject responseJO = jsonObject.getAsJsonObject("response");
                    if (responseJO.isJsonObject() && responseJO.has("photos")) {
                        JsonObject photos = responseJO.getAsJsonObject("photos");
                        if (photos.isJsonObject() && photos.has("items")) {
                            JsonArray items = photos.getAsJsonArray("items");
                            if (items.isJsonArray()) {
                                JsonObject photo = items.get(0).getAsJsonObject();
                                if (photo.has("prefix") && photo.has("suffix")) {
                                    String photoUrl = photo.get("prefix").getAsString() + "160x160" + photo.get("suffix").getAsString();
                                    if (listener != null) {
                                        Map<String, Object> result = new HashMap<>();
                                        result.put("venueId", venueId);
                                        result.put("photoUrl", photoUrl);
                                        listener.onFoursquareRequestFinished(FoursquareServiceType.getVenuePhoto, result, null);
                                    }
                                } else {
                                    if (listener != null) {
                                        Map<String, Object> result = new HashMap<>();
                                        result.put("venueId", venueId);
                                        result.put("photoUrl", "");
                                        listener.onFoursquareRequestFinished(FoursquareServiceType.getVenuePhoto, result, null);
                                    }
                                }
                                return;
                            }
                        }
                    }
                }

                if (listener != null) {
                    listener.onFoursquareRequestFinished(FoursquareServiceType.getVenuePhoto, null, "Ocurrió un error al intentar obtener las fotos de los establecimientos");
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (listener != null) {
                    listener.onFoursquareRequestFinished(FoursquareServiceType.searchVenues, null, "El servidor de Foursquare no pudo ser contactado para obtener las fotos de los establecimientos");
                }
            }
        });
    }
}
