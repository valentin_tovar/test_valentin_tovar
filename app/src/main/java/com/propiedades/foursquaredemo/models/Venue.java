package com.propiedades.foursquaredemo.models;

public class Venue {

    private String venueId;
    private String venueName;
    private String address;
    private String crossStreet;
    private String city;
    private String mainCategoryName;
    private String photoUrl;

    public Venue(String venueId, String venueName, String address, String crossStreet, String city, String mainCategoryName) {
        this.venueId = venueId;
        this.venueName = venueName;
        this.address = address;
        this.crossStreet = crossStreet;
        this.city = city;
        this.mainCategoryName = mainCategoryName;
    }

    public String getVenueId() {
        return venueId;
    }

    public String getVenueName() {
        return venueName;
    }

    public String getAddress() {
        return address;
    }

    public String getCrossStreet() {
        return crossStreet;
    }

    public String getCity() {
        return city;
    }

    public String getMainCategoryName() {
        return mainCategoryName;
    }

    public String getPhotoUrl() { return photoUrl; }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
