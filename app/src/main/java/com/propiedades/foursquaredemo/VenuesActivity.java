package com.propiedades.foursquaredemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.propiedades.foursquaredemo.controllers.RecyclerViewAdapter;
import com.propiedades.foursquaredemo.models.Venue;
import com.propiedades.foursquaredemo.views.AlertDialogFragment;
import com.propiedades.foursquaredemo.views.BasicAlertDialogFragment;
import com.propiedades.foursquaredemo.web.FoursquareRequest;

import java.util.ArrayList;
import java.util.Map;

public class VenuesActivity extends AppCompatActivity implements FoursquareRequest.FoursquareRequestListener {

    private RecyclerViewAdapter recyclerViewAdapter;
    private ArrayList<Venue> venues = new ArrayList<>();
    private int searchPhotoVenueIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venues);

        setupRecyclerView();
        initVenues();
    }

    private void setupRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerViewAdapter = new RecyclerViewAdapter(this, venues);
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initVenues() {
        FoursquareRequest foursquareRequest = new FoursquareRequest();
        foursquareRequest.setListener(this);
        foursquareRequest.searchVenues();
    }

    private void loadNextPhoto() {
        if (searchPhotoVenueIndex < venues.size()) {
            FoursquareRequest foursquareRequest = new FoursquareRequest();
            foursquareRequest.setListener(this);
            foursquareRequest.getVenueMainPhoto(venues.get(searchPhotoVenueIndex).getVenueId());
        }
    }

    @Override
    public void onFoursquareRequestFinished(FoursquareRequest.FoursquareServiceType serviceType, Map<String, Object> result, String errorMessage) {

        if (serviceType == FoursquareRequest.FoursquareServiceType.searchVenues) {
            ProgressBar progressBar = findViewById(R.id.progressBar);
            progressBar.setVisibility(View.INVISIBLE);
            if (errorMessage != null) {
                AlertDialogFragment alert = new AlertDialogFragment();

                Bundle arguments = new Bundle();
                arguments.putString("title", "Error");
                arguments.putString("message", errorMessage);
                arguments.putString("cancelButtonLabel", "Reintentar");
                alert.setArguments(arguments);

                alert.setListener(new AlertDialogFragment.AlertDialogFragmentListener() {
                    @Override
                    public void okButtonTapped() { }

                    @Override
                    public void cancelButtonTapped() {
                        initVenues();
                    }
                });

                alert.show(getSupportFragmentManager(), "AlertDialogFragment");
            } else {
                if (result.containsKey("venues")) {
                    this.venues = (ArrayList<Venue>) (result.get("venues"));

                    recyclerViewAdapter.setVenues(venues);
                    recyclerViewAdapter.notifyDataSetChanged();

                    loadNextPhoto();
                }
            }
        } else if (serviceType == FoursquareRequest.FoursquareServiceType.getVenuePhoto) {
            if (errorMessage != null) {
                BasicAlertDialogFragment alert = new BasicAlertDialogFragment();

                Bundle arguments = new Bundle();
                arguments.putString("title", "Error");
                arguments.putString("message", errorMessage);
                alert.setArguments(arguments);

                alert.show(getSupportFragmentManager(), "BasicAlertDialog");

            } else {
                String photoUrl = (String)(result.get("photoUrl"));

                if (photoUrl != null && photoUrl.length() > 0) {
                    venues.get(searchPhotoVenueIndex).setPhotoUrl(photoUrl);
                    recyclerViewAdapter.setVenues(venues);
                    recyclerViewAdapter.notifyDataSetChanged();
                }

                searchPhotoVenueIndex++;
                loadNextPhoto();
            }
        }
    }
}
